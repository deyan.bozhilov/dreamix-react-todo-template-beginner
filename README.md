# Dreamix React Todo App

## Beginner Template

In this template we have laid out all of the project files and structure you will need to complete all tasks.

We are focusing on Reacts logic, data manipulation and data fetching setup and good practices.

The design is mostly pre-made for you with some exceptions, where you will need to figure things out for yourself.

In most files you have comments explaining what you need to do or what is done for you. Read through them carefully and implement your solutions according to them. Thats how you will get the most of the course.

## Development

Before you run the project for the first time, you need to install all the dependencies. In you terminal run:

```
npm i
```

then

```
npm run dev
```

This command will open your default browser to `localhost:3000` and you can start going from there.

## OR

To run it with docker:

```
docker compose up
```

- Browser won't automatically open if using docker, you need to manually go to `localhost:3000`
- Also make sure port 3000 is free as it is the only exported port

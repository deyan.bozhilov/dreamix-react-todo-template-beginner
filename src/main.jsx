// Quick Overview of the React syntax and terminology
// -----------------------------------------------
// TERM: JSX
// <Component></Component>  -  This is a JSX component, which looks like HTML, but is actually JavaScript
// ^---------------------^  - it has an opening and closing tag <...> and </...>
// ( Sometimes it can be self closing <Component /> if nothing is passed in as children )

// TERM: PASSING PROPS
// <Component prop="value" />  -  This is a JSX component with a prop, which is a value that is passed to the component
// ^---------------------^  -  The prop is passed as an attribute to the component and can be different types of values:
// ( string, number, boolean, object, array, function, etc. )

// TERM: COMPONENT
// To create a component, you can use a function that returns JSX:
// function Component() {
//   return <div>Component</div>;
// }
// ---- OR ----
// const Component = () => {
//   return <div>Component</div>;
// };

// TERM: REACTIVITY and CONSUMING PROPS
// Inside the component you can use the prop value:
// function Component({ prop }) {
//   return <div>{prop}</div>;
// }
// Each time the value of the prop changes, the component is re-rendered and updated to display the new value
// ( This is called reactivity )
// Thats why it is important to think what you pass as props to your components and if they are really needed
// as if you overdo it, you will end up with a lot of unnecessary re-renders and hurt performance

// Another thing we will use constantly are hooks
// TERM: HOOKS
// Hooks are functions that allow you to use React features inside your components
// They are used to consume context, make requests, etc.
// They are always named with the prefix use, like useAuth, useRegister, etc.
// They are always called inside a component, like this:
// function Component() {
//   const { prop } = useAuth();
//   return <div>{prop}</div>;
// }

// These are the most basic and common terms that will help you communicate effectively with your team
// For more information, check out the official React documentation: https://reactjs.org/docs/getting-started.html
// or watch the youtube videos from the course: https://www.youtube.com/playlist?list=PL6i_MjauxZk_S76OD3OFDWahzVcF35zEj

// 🚀 ⚛ Good luck on your journey to become a React developer! ⚛ 🚀

import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, createRoutesFromElements, Route, RouterProvider } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";

import { PrivateRoute } from "./components/auth";
import { Login, Register } from "./pages/auth";
import { TodoWrapper } from "./pages/layout";
import { AddTodo, Dashboard, EditTodo } from "./pages/todos";
import { NotFound } from "./components";
import { AuthProvider, ThemeProvider, TodoControlsProvider } from "./context";
import { prepareMocks } from "./mocks";

import "./index.css";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: 1,
      refetchOnWindowFocus: false,
    },
  },
});

const router = createBrowserRouter(
  createRoutesFromElements(
    // The structure of the app routes is as follows:
    // "/login", "/register" - are public routes, which can be accessed without being logged in
    // --------------------------------------------------------------
    // All other routes are private routes, which can only be accessed if the user is logged in
    // "/" - is the dashboard, where you can see all your todos
    // "/add", "/edit/:id" - are routes for adding and editing todos
    // "*" - is the 404 page, which is shown if the page you are trying to access is not found
    <Route path='/'>
      {/* Auth pages */}
      <Route path='/register' element={<Register />} />
      <Route path='/login' element={<Login />} />
      {/* All other routes of our app are wrapped with a <PrivateRoute>, which internally checks if there is a token  */}
      {/* in localStorage and if the user is currently logged in. If not, you are redirected to the login page. */}
      <Route
        path='/'
        element={
          <PrivateRoute>
            {/* Wrapper, which has the page layout ( navbar and outlet where the route pages will be rendered ) */}
            <TodoWrapper />
          </PrivateRoute>
        }
      >
        <Route
          index
          element={
            <TodoControlsProvider>
              {/* This is a custom provider, which exposes methods to filter, sort and search todos */}
              <Dashboard />
            </TodoControlsProvider>
          }
        />
        <Route path='add' element={<AddTodo />} />
        <Route path='edit/:id' element={<EditTodo />} />
        <Route path='*' element={<NotFound />} />
      </Route>
    </Route>
  )
);

const root = document.getElementById("root");

prepareMocks().then(() => {
  if (root) {
    ReactDOM.createRoot(root).render(
      <React.StrictMode>
        <QueryClientProvider client={queryClient}>
          {/* For authentication we want to store all the necessary data in context, so it can be available anywhere */}
          {/* Also if we change something, we want to trigger a re-render and update the content on screen */}
          <AuthProvider>
            {/* For our dark mode, we have a theme provider, that stores out current theme and exposes methods to change it */}
            <ThemeProvider>
              {/* Routes are already defined, they are simply strings, that will be in the url and for that url -> show me a specific page */}
              <RouterProvider router={router} />
            </ThemeProvider>
          </AuthProvider>
          <ReactQueryDevtools />
        </QueryClientProvider>
      </React.StrictMode>
    );
  }
});

import { createContext, useContext, useMemo, useReducer } from "react";

const TodoControlsContext = createContext();

// We set our initial state
const initialState = {
  search: "",
  done: null,
  viewMode: "list",
  priority: "",
};

// We indicate the state, we want to be able to reset
const trackableInitialState = {
  search: "",
  done: null,
  priority: "",
};

// We add all the available actions that we will handle in the reducer
// NOTE: It is good practice to do it like this, because it will help you avoid typos and it will be easier to refactor
const TODO_CONTROLS_TYPES = {
  SEARCH: "SEARCH",
  DONE: "DONE",
  PRIORITY: "PRIORITY",
  RESET: "RESET",
  VIEW_CHANGE: "VIEW_CHANGE",
};

// We create an object will all the outcomes of each action
const actions = {
  [TODO_CONTROLS_TYPES.SEARCH]: (state, action) => ({ ...state, search: action.payload }),
  [TODO_CONTROLS_TYPES.DONE]: (state, action) => ({ ...state, done: action.payload }),
  [TODO_CONTROLS_TYPES.PRIORITY]: (state, action) => ({ ...state, priority: action.payload }),
  [TODO_CONTROLS_TYPES.RESET]: (state) => ({ ...state, ...trackableInitialState }),
  [TODO_CONTROLS_TYPES.VIEW_CHANGE]: (state, action) => {
    // We try to set the viewMode in localStorage, if it fails, we still return the new state
    // it will just not be saved in localStorage and will be reverted to the default value on refresh
    try {
      localStorage.setItem("viewMode", action.payload);
      return { ...state, viewMode: action.payload };
    } catch (error) {
      return { ...state, viewMode: action.payload };
    }
  },
};

function reducer(state, action) {
  // We check if the action type is handled in the actions object
  const handler = actions?.[action.type];
  if (handler) {
    // If it is, we call the handler and return the new state
    return handler(state, action);
  }
  // Otherwise we throw an error
  throw new Error("Not handled action type!");
}

function reducerInitializer(initialState) {
  // We try to get the viewMode from localStorage and apply it to our filters, if it fails, we still return the new state
  try {
    const viewMode = localStorage.getItem("viewMode");
    return {
      ...initialState,
      viewMode: viewMode ?? initialState.viewMode,
    };
  } catch (error) {
    return initialState;
  }
}

function TodoControlsProvider({ children }) {
  // We use the useReducer hook to initialize the state as it is a better practice for handling multiple state values
  const [state, dispatch] = useReducer(reducer, initialState, reducerInitializer);

  const { search, done, priority } = state;

  // We use the useMemo hook to memoize the value of the context and prevent it from re-creating on every render,
  // just when the values in the dependency array are changed
  const isTouched = useMemo(
    () => JSON.stringify({ search, done, priority }) !== JSON.stringify(trackableInitialState),
    [search, done, priority]
  );

  const value = useMemo(() => ({ state, dispatch, isTouched }), [state, dispatch, isTouched]);

  return <TodoControlsContext.Provider value={value}>{children}</TodoControlsContext.Provider>;
}

function useTodoControls() {
  const context = useContext(TodoControlsContext);
  if (typeof context === "undefined") {
    throw new Error("useTodoControls must be used within a TodoControlsProvider");
  }
  return context;
}

export { TODO_CONTROLS_TYPES, TodoControlsProvider, useTodoControls };

import { createContext, useContext, useEffect, useMemo, useState } from "react";

import { getTokenFromStorage } from "../utils";

// We initialize the context with a default value (undefined)
const AuthContext = createContext();

// We set our initial state
const initialAuthState = {
  token: null,
  user: null,
  isAuthenticated: false,
};

// We create a provider component that will wrap our app and provide the context value
function AuthProvider({ children }) {
  // We use the useState hook to initialize the state
  const [state, setState] = useState(initialAuthState);

  // We use the useEffect hook to check if there is a token in localStorage on mount
  // ( as the provider is wrapping our whole app, which will only mount once )
  useEffect(() => {
    const token = getTokenFromStorage();
    if (token) {
      setState((prev) => ({ ...prev, token }));
    }
  }, []);

  // We use the useMemo hook to memoize the value of the context and prevent it from re-creating on every render
  // NOTE: We don't really need it in this case, but it's good practice to use it when you have a context that has state
  const value = useMemo(() => [state, setState], [state, setState]);

  // We return the provider with the value of the context
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

// We create a custom hook to use the context
function useAuth() {
  const context = useContext(AuthContext);
  // We throw an error if the context is undefined, which means that the hook is used outside of the provider and
  // the value we passed to the provider has not been set.
  if (typeof context === "undefined") {
    throw new Error("useAuth must be used within a AuthProvider");
  }
  // Otherwise we return the context
  return context;
}

export { AuthProvider, initialAuthState, useAuth };

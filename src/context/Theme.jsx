import { createContext, useCallback, useContext, useEffect, useMemo, useState } from "react";

import { useLocalStorage, useMedia } from "../hooks";

// We create a context for the theme
const ThemeContext = createContext();

// We create a provider component that will wrap our app and provide the context value
function ThemeProvider({ children }) {
  // We use the useLocalStorage hook to get the stored theme and a function to set it
  // Storing the theme value in localStorage will allow us to persist the theme even if the user refreshes the page
  const [storedTheme, setStoredTheme, removeStoredTheme] = useLocalStorage("theme", null);
  // We use the useMedia hook to get the user's preferred theme
  const userPrefersDarkMode = useMedia("(prefers-color-scheme: dark)");
  // We use the useState hook to initialize the local state, which we will use to compare the stored theme with the user's preferred theme
  const [theme, setTheme] = useState();

  useEffect(() => {
    // If there is no stored theme, we set the theme to the user's preferred theme
    // We do this on mount, because we want to set the theme to the user's preferred theme on first load
    // We do it to the local theme only and not to the stored theme, because we don't want to persist the
    //  user's preferred theme, which can change
    if (!storedTheme) {
      return setTheme(userPrefersDarkMode ? "dark" : "light");
    }
    // Otherwise we set the theme to the stored theme
    return setTheme(storedTheme);
  }, [userPrefersDarkMode, storedTheme, removeStoredTheme]);

  const toggleTheme = useCallback(
    () =>
      setStoredTheme((prev) => {
        // If there is no stored theme, we set the theme to the opposite of the user's preferred theme
        if (!prev) {
          return userPrefersDarkMode ? "light" : "dark";
        }
        // Otherwise we toggle the theme
        return prev === "dark" ? "light" : "dark";
      }),
    [userPrefersDarkMode, setStoredTheme]
  );

  const resetTheme = useCallback(() => {
    // We remove the stored theme and set the theme to the user's preferred theme if the resetTheme method is called
    Promise.resolve(removeStoredTheme()).then(() => setTheme(userPrefersDarkMode ? "dark" : "light"));
  }, [removeStoredTheme, userPrefersDarkMode]);

  // We use the useCallback hook to memoize the setSpecificTheme method, so it doesn't get re-created on every render
  const setSpecificTheme = useCallback((value) => setStoredTheme(value), [setStoredTheme]);

  useEffect(() => {
    // We add the theme class to the document element on theme change, so that tailwind can apply the correct styles
    if (theme === "dark") {
      return document.documentElement.classList.add(theme);
    }
    document.documentElement.classList.remove("dark");
  }, [theme]);

  // We use the useMemo hook to memoize the value of the context and prevent it from re-creating on every render
  const value = useMemo(
    () => ({
      theme,
      storedTheme,
      toggleTheme,
      resetTheme,
      setSpecificTheme,
    }),
    [theme, toggleTheme, resetTheme, setSpecificTheme, storedTheme]
  );

  return <ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>;
}

function useTheme() {
  const context = useContext(ThemeContext);
  if (typeof context === "undefined") {
    throw new Error("useTheme must be used within a ThemeProvider");
  }
  return context;
}

export { ThemeProvider, useTheme };

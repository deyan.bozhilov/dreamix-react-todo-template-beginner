import { Link } from "react-router-dom";

import { Alert, Button } from "../../components";
import { useAuth } from "../../context";
import { useRegister } from "../../hooks/auth";
import { setTokenInStorage } from "../../utils";
import { AuthLayout } from "../layout";

import { AuthForm } from "./components";
import { withAuth } from "./withAuth";

function Register() {
  const [, setAuth] = useAuth();

  // We use the useRegister hook to register a new user
  const { register, isLoading, error } = useRegister({
    onSuccess: (res) => {
      // If the request is successful,
      // we store the token in localStorage
      setTokenInStorage(res.token);
      // and update the auth context
      setAuth((prev) => ({ ...prev, token: res.token }));
    },
  });

  const responseError = error?.response?.data?.message;

  return (
    <AuthLayout title='Create an account'>
      {/* As our login is pretty much the same, we can extract it in a different component and just pass in the */}
      {/* values that are different for the Register ( the onSubmit method and the buttons and links )  */}
      {/* -------------------------------------------------------------------------------------------- */}
      {/* NOTE: We could have passed in the error and visualize it from there, but it is just a personal preference */}
      {/* to have it closer to the request. You can do it if you feel like it looks better to you. */}
      <AuthForm onSubmit={register}>
        <div className='mt-6 space-y-4 text-center'>
          {/* We add a conditional render for the Alert error, in case some error from the back-end comes up */}
          {responseError && <Alert>{responseError}</Alert>}
          <Button className='w-full sm:w-auto' isLoading={isLoading}>
            Create Account
          </Button>
          <p className='text-sm'>
            Already Have An Account?{" "}
            <Link to='/login' className='cursor-pointer whitespace-nowrap underline'>
              Sign In
            </Link>
          </p>
        </div>
      </AuthForm>
    </AuthLayout>
  );
}

export default withAuth(Register);

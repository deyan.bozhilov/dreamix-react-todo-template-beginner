import { Navigate } from "react-router-dom";

import { useAuth } from "../../context";

export function withAuth(Component) {
  return function AuthComponent(props) {
    const [{ token }] = useAuth();
    // Check if there is a token in the context and redirect to the home page, so you can't access the
    // login or register page if logged in
    if (token) {
      return <Navigate to='/' replace />;
    }

    return <Component {...props} />;
  };
}

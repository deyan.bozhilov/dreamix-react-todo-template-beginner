import { useForm } from "react-hook-form";
import PropTypes from "prop-types";

import { Input, Password } from "../../../components/forms";

const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/g;

export default function AuthForm({ onSubmit, children }) {
  // We use the useForm hook to handle the form state and the validation
  // -----------------------------------------------------------------
  // IMPORTANT: DO NOT confuse this register with the one from the useRegister hook
  // This one is from the react-hook-form library and is used to register the inputs, not the user
  // -----------------------------------------------------------------
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  return (
    <form className='flex flex-col gap-4' onSubmit={handleSubmit(onSubmit)}>
      {/* 
        We have 2 inputs on which we give names: {...register('input-name')} and 
        as a second argument to the register function, we pass in validation rules

        As our Input component is setup to handle errors,
        ( file: ./src/components/forms/inputs/Input.jsx wrapped with 'withInputWrapper' ) 
        we can pass in the error prop to it, which will be the error message from the errors object
      */}
      <Input
        label='Username'
        placeholder='Enter username'
        error={errors?.username}
        {...register("username", {
          required: { value: true, message: "Field is required!" },
          minLength: { value: 2, message: "Minimum 2 characters!" },
          maxLength: { value: 50, message: "Maximum 50 characters!" },
        })}
      />
      {/* 
        For more info on the validations we are using, see: https://react-hook-form.com/api/useform/register/ and 
        and check the radio button for register with validation messages
      */}
      <Password
        label='Password'
        placeholder='Enter password'
        error={errors?.password}
        {...register("password", {
          required: { value: true, message: "Field is required!" },
          pattern: {
            value: passwordRegex,
            message: "Password must be minimum 8 characters, 1 uppercase letter, 1 lowercase letter and 1 number!",
          },
        })}
      />
      {children}
    </form>
  );
}

// Prop types are used to simply yell at you if you are expecting a specific type of prop, but passing in something else
// This is not as good as Typescript type safety, but it will do for our course. When you advance with React and understand
// the core concepts, you can always add Typescript to your project
AuthForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  children: PropTypes.node,
};

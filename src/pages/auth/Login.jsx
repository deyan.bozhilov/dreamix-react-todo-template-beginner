// HINT: These are all the imports you need for this component
// import { Link } from "react-router-dom";

// import { Alert, Button } from "../../components";
// import { useAuth } from "../../context";
// import { useLogin } from "../../hooks/auth";
// import { setTokenInStorage } from "../../utils";
// import { AuthLayout } from "../layout";

// import { AuthForm } from "./components";
import { withAuth } from "./withAuth";

function Login() {
  // TODO:
  // Now after you have registered, you want to be able to login.
  // As we saw in the Register component, we can use the useAuth hook to get the setAuth method,
  // which we can use to update the token in the context.

  // Following the same pattern as in the register page, we need use the useLogin hook to login a user
  // If the request has been successful, we need to store the token in localStorage and update the auth context
  // TODO: Add hook logic here

  // We also need to get the error message from the response, if there is one

  // And render the same form as in the Register component, but with the login buttons, title and link to te register page
  return <>Login</>;
}

export default withAuth(Login);

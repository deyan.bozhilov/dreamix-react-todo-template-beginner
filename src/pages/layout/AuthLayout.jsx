import { Card, Container } from "../../components";

export default function AuthLayout({ title, children }) {
  return (
    <div className='grid min-h-screen overflow-x-hidden bg-gradient-to-b from-gray-600 to-gray-400 p-4 dark:from-gray-900 dark:to-gray-700'>
      <Container className='relative isolate grid h-full place-items-center'>
        <div className='absolute -left-16 -top-4 h-60 w-60 rotate-45 rounded-xl bg-slate-400 dark:bg-slate-700' />
        <div className='absolute right-12 top-4 hidden h-40 w-40 rounded-full bg-slate-400 dark:bg-slate-700 md:block' />
        <div className='absolute bottom-20 left-10 hidden h-40 w-20 rotate-45 rounded-full bg-slate-300 dark:bg-slate-600 md:block' />
        <div className='absolute -right-10 bottom-6 h-48 w-48 rotate-12 rounded-xl bg-slate-300 dark:bg-slate-600' />
        <Card className='z-10 w-full max-w-md rounded-2xl px-6 py-12 md:p-12'>
          {title && <h1 className='mb-4 text-center text-3xl font-bold'>{title}</h1>}
          {children}
        </Card>
      </Container>
    </div>
  );
}

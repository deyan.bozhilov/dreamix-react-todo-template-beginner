import { Switch } from "../../../components";
import { Dark, Light, SystemColor } from "../../../components/icons";
import { useTheme } from "../../../context";

export default function ThemeSwitch() {
  const { storedTheme, setSpecificTheme, resetTheme } = useTheme();

  const isSystemTheme = storedTheme === null;
  const isLightTheme = storedTheme === "light";
  const isDarkTheme = storedTheme === "dark";

  const options = [
    {
      icon: SystemColor,
      activeCondition: isSystemTheme,
      onClick: resetTheme,
      title: "Use system colors",
    },
    {
      icon: Light,
      activeCondition: isLightTheme,
      onClick: () => setSpecificTheme("light"),
      title: "Use light theme",
    },
    {
      icon: Dark,
      activeCondition: isDarkTheme,
      onClick: () => setSpecificTheme("dark"),
      title: "Use dark theme",
    },
  ];

  return (
    <Switch
      options={options}
      indicatorClassName={{
        "translate-x-0": isSystemTheme,
        "translate-x-10": isLightTheme,
        "translate-x-20": isDarkTheme,
      }}
    />
  );
}

import { Button } from "../../../components";

import Dropdown from "./Dropdown";

export default {
  title: "components/layout/Dropdown",
  component: Dropdown,
};

const Template = (args) => (
  <div className='flex w-full justify-end bg-white p-4 dark:bg-slate-800'>
    <div className='grow' />
    <Dropdown {...args} />
  </div>
);

const inputProps = {
  trigger: <Button className='ml-auto'>Trigger</Button>,
  children: <p>Opala kjhjhbn</p>,
};

export const InputExample = { args: inputProps, render: Template };

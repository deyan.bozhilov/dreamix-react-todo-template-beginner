import cn from "classnames";

const dropdownClasses = [
  "absolute flex flex-col space-y-4",
  "top-10 right-0",
  "opacity-0 origin-top-right scale-0",
  "group-hover:scale-100 group-hover:opacity-100",
  "group-focus-within:opacity-100 group-focus-within:scale-100",
  "dark:bg-slate-800 bg-white",
  "rounded-lg p-4 border-2 border-slate-300 dark:border-slate-700",
  "transition-all",
];

export default function Dropdown({ trigger, children }) {
  return (
    <div className='group relative h-min'>
      {trigger}
      <div className={cn(dropdownClasses)}>{children}</div>
    </div>
  );
}

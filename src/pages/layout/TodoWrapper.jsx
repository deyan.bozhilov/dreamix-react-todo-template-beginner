import { Outlet, ScrollRestoration } from "react-router-dom";

import { Container } from "../../components";

import Navbar from "./Navbar";

export default function TodoWrapper() {
  return (
    <>
      <Navbar />
      <main className='px-4'>
        <Container>
          {/* 
            Outlet is telling the React router where to render all the pages depending on the current URL. 
            If this is not added, you will see a blank screen 
          */}
          <Outlet />
          {/* 
            If you have a lot of content, and go from page to page, you might want to keep your scroll position
            NOTE: This is not always the case, so you can add or remove this component as you see fit
          */}
          <ScrollRestoration />
        </Container>
      </main>
    </>
  );
}

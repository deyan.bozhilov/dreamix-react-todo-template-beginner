import { Link, useNavigate } from "react-router-dom";
import { useQueryClient } from "@tanstack/react-query";

import { Button, Container } from "../../components";
import { ChevronDown, Logout } from "../../components/icons";
import { initialAuthState, useAuth, useTheme } from "../../context";
import { useAccount, useLogout } from "../../hooks/auth";

import { Dropdown, ThemeSwitch } from "./components";

const themeLabel = {
  light: "Light",
  dark: "Dark",
  null: "System",
};

export default function Navbar() {
  const queryClient = useQueryClient();
  const navigate = useNavigate();

  const [{ user, token }, setAuth] = useAuth();
  const { storedTheme } = useTheme();

  const { isFetching } = useAccount({
    // We only fetch the account if the token is set
    enabled: Boolean(token),
    // We set the onSuccess function to set the auth state on success
    onSuccess: (user) => setAuth((prev) => ({ ...prev, user, isAuthenticated: true })),
    // We set the onError function to navigate to the login page on error
    onError: () => navigate("/login"),
    // We set the staleTime and cacheTime to Infinity so that the query will never be invalidated (only on page refresh or manually)
    staleTime: Infinity,
    cacheTime: Infinity,
  });

  const { logout } = useLogout({
    onSuccess: () => {
      // Clear the cache and set the auth state to the initial state
      setAuth(initialAuthState);
      queryClient.clear();
    },
  });

  return (
    <nav className='sticky top-0 z-10 h-14 bg-slate-100 px-4 shadow-lg dark:bg-slate-800'>
      <Container className='flex h-full items-center space-x-2'>
        <div className='flex h-full grow items-center'>
          <Link to='/' className='text-lg font-semibold'>
            React Todo
          </Link>
        </div>
        <Dropdown
          trigger={
            <Button variant='outline' size='sm' isLoading={isFetching}>
              <span className='max-w-[10ch] truncate capitalize sm:max-w-[20ch]'>{isFetching ? "Loading..." : user?.username}</span>
              <ChevronDown className='h-5 w-5 shrink-0' />
            </Button>
          }
        >
          <div className='flex flex-col'>
            <span className='text-sm'>Theme: {themeLabel[storedTheme]}</span>
            <ThemeSwitch />
          </div>
          <hr className='border-t-slate-300 dark:border-t-slate-600' />
          <Button size='sm' variant='danger' className='inline-flex items-center space-x-2' onClick={logout}>
            <Logout className='h-5 w-5 shrink-0' />
            <span>Logout</span>
          </Button>
        </Dropdown>
      </Container>
    </nav>
  );
}

import { useNavigate } from "react-router-dom";

import { Alert } from "../../components";
import { useTodoAdd } from "../../hooks/todo";

import { TodoForm } from "./modules";

export default function AddTodo() {
  const navigate = useNavigate();

  const { add, isLoading, error } = useTodoAdd({
    onSuccess: () => {
      // We navigate to the home page if the mutation was successful
      navigate("/");
    },
  });

  // Same as the login and register pages, we get the error from the response
  const responseError = error?.response?.data?.message;

  return (
    <TodoForm title='Add Todo' onSubmit={add} isLoading={isLoading}>
      {responseError && <Alert>{responseError}</Alert>}
    </TodoForm>
  );
}

export default function EditTodo() {
  // TODO: You first need to get the id from the url params
  // You can use the useParams hook from react-router-dom

  // Then you need to get the todo by id
  // While fetching it, show a spinner and load the NotFound component if the todo is not found

  // Do not show the form if the todo is not yet fetched as you need to pass the default values to the form
  // NOTE: if you mount the form before the todo is fetched, the form will be uncontrolled and you will not be able to edit the todo

  // After the todo is edited and you are ready to submit the form, pass the mutation function to the onSubmit prop of the TodoForm component
  // and navigate to the home page if the mutation was successful

  // NOTE: Don't forget the error handling

  return <>Edit Todo</>;
}

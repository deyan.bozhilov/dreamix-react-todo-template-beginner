import { Link } from "react-router-dom";

import { Button } from "../../components";
import { Plus } from "../../components/icons";

// 🧠 Challenge 1: How to make the filter work?
// TODO: filter functions can be defined outside of the component to avoid re-creating them on every render
// 1. We need a filter function for the title
// 2. We need a filter function for the priority
// 3. We need a filter function for the completed state

export default function Dashboard() {
  // TODO: We need to get the state from the TodoControls context

  // TODO: We can defer the title filter value to delay the state update if the user is typing and there are not enough resources to compute the state update
  // Here is a good article about the useDeferredValue hook: https://blog.webdevsimplified.com/2022-05/use-deferred-value/

  // TODO: We get out todos from the "DB"
  // const { todos, isFetching } = useTodos();

  // TODO: We should memoize the todos as the filter updates will trigger a re-render so we don't want to re-compute the todos
  // TODO: We now need to apply all the filters to the todos. But we need to make sure that the filters are applied together.
  // So one way you can do it is to use the reduce method on the filters array and pass the todos as the initial value.
  // Then reduce an array of the filter functions, so that you can apply each filter to the previous result.
  // 🧠 Challenge 2: How to apply multiple filters on our data array?

  // NOTE: add useMemo where necessary to prevent unnecessary re-renders

  return (
    <div className='isolate py-8'>
      <div className='flex items-start gap-4'>
        <div className='grow'>
          <h1 className='mb-2 text-2xl font-semibold md:text-4xl'>Welcome back!</h1>
          <h2 className='text-base italic opacity-70'>{"Complete todos and let's get some dopamine :)"}</h2>
        </div>

        <div className='fixed bottom-8 right-4 z-10 md:static'>
          <Button
            as={Link}
            to='add'
            size='none'
            title='Add a todo'
            className='h-14 w-14 rounded-full md:h-auto md:w-auto md:rounded-md md:px-6 md:py-3'
          >
            <Plus className='h-8 w-8 grow-0 md:h-5 md:w-5' />
            <span className='hidden md:block'>Add Todo</span>
          </Button>
        </div>
      </div>

      {/* TODO: you can render the controls here */}

      <div className='relative'>
        {/* TODO: Render the TodoList here with all the finalized data */}
        {/* You can show a loading indicator here with the Spinner component */}
      </div>
    </div>
  );
}

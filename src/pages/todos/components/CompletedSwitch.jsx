import { Switch } from "../../../components";
import { Check, TodoList, X } from "../../../components/icons";
import { TODO_CONTROLS_TYPES, useTodoControls } from "../../../context";

export default function CompletedSwitch() {
  const { state, dispatch } = useTodoControls();

  const isAll = state.done === null;
  const isDone = state.done === true;
  const isNotDone = state.done === false;

  const options = [
    {
      icon: TodoList,
      activeCondition: isAll,
      onClick: () => dispatch({ type: TODO_CONTROLS_TYPES.DONE, payload: null }),
      title: "View all todos",
    },
    {
      icon: Check,
      activeCondition: isDone,
      onClick: () => dispatch({ type: TODO_CONTROLS_TYPES.DONE, payload: true }),
      title: "View completed todos only",
    },
    {
      icon: X,
      activeCondition: isNotDone,
      onClick: () => dispatch({ type: TODO_CONTROLS_TYPES.DONE, payload: false }),
      title: "View incomplete todos only",
    },
  ];

  return (
    <Switch
      keyPrefix='completed_todos'
      options={options}
      indicatorClassName={{
        "translate-x-0": isAll,
        "translate-x-10": isDone,
        "translate-x-20": isNotDone,
      }}
    />
  );
}

import cn from "classnames";

const base = [
  "transition-all",
  "cursor-pointer",
  "focus:ring !ring-green-600 dark:ring-offset-slate-900",
  "border-2 outline-none ring-offset-2",
  "border-slate-300 dark:border-slate-500",
  "dark:checked:border-green-600 dark:checked:hover:border-green-700",
  "dark:bg-slate-600 dark:hover:bg-slate-700",
  "dark:checked:bg-green-600 dark:checked:hover:bg-green-700",
  "bg-slate-100 hover:bg-slate-200",
  "checked:text-green-600 checked:hover:text-green-700",
];

const sizes = {
  default: "md:h-8 md:w-8 h-6 w-6 rounded-lg",
  sm: "md:h-5 md:w-5 h-4 w-4 rounded-md",
};

export default function TodoToggle({ className, size = "default", checked, onChange, id }) {
  return <input className={cn(base, sizes[size], className)} type='checkbox' id={id} checked={checked} onChange={onChange} />;
}

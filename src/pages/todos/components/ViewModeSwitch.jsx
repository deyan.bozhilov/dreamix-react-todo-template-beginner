import { Switch } from "../../../components";
import { Grid, List } from "../../../components/icons";
import { TODO_CONTROLS_TYPES, useTodoControls } from "../../../context";

export default function ViewModeSwitch() {
  const { state, dispatch } = useTodoControls();

  const isList = state.viewMode === "list";
  const isGrid = state.viewMode === "grid";

  const options = [
    {
      icon: List,
      activeCondition: isList,
      onClick: () => dispatch({ type: TODO_CONTROLS_TYPES.VIEW_CHANGE, payload: "list" }),
      title: "View todos in list mode",
    },
    {
      icon: Grid,
      activeCondition: isGrid,
      onClick: () => dispatch({ type: TODO_CONTROLS_TYPES.VIEW_CHANGE, payload: "grid" }),
      title: "View todos in grid mode",
    },
  ];

  return (
    <Switch
      keyPrefix='view_mode_todos'
      options={options}
      indicatorClassName={{
        "translate-x-0": isList,
        "translate-x-10": isGrid,
      }}
    />
  );
}

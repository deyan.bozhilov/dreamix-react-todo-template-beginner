import cn from "classnames";

const PRIORITY_THEME = {
  low: "bg-blue-600 text-white",
  mid: "bg-orange-400 text-black",
  high: "bg-red-600 text-white",
};

export default function TodoPriorityBadge({ priority }) {
  return (
    <span
      className={cn(
        "absolute -top-3 left-4 rounded-full px-2 py-0.5 text-sm capitalize",
        PRIORITY_THEME[priority],
        priority === "high" && "animate-bounce"
      )}
      title={priority}
    >
      Priority: {priority}
    </span>
  );
}

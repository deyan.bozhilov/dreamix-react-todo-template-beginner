export default function SubTodoItem({ _todo, _subTodo, _onDeleteSubTodo, _onEditSubTodo }) {
  // TODO: You have to implement the onEditSubTodo method by passing the todo with the updated subTodo done status

  // As we did in the TodoItem you can make the wrapper with a label element, so that wherever you click on the subTodo it will toggle the completed status
  // use the TodoToggle component for the toggle
  // use similar styling as in the parent ( line through if completed )
  // Add a delete button for the subTodo
  // NOTE: Make sure you are not triggering 2 requests with the delete button (mark as complete + delete request), if so you can think of a wat to prevent it
  return <>SubTodo Item</>;
}

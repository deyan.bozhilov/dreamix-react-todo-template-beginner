import cn from "classnames";

const collapseBase = [
  "group overflow-hidden",
  "relative flex flex-col",
  "rounded-2xl border-2 border-transparent open:border-gray-300 dark:open:border-gray-600",
  "bg-slate-100 dark:bg-slate-900",
  "transition-all",
];

const collapseTriggerBase = [
  "flex items-center gap-2 [&::-webkit-details-marker]:hidden select-none",
  "rounded-t-2xl rounded-b-2xl group-open:rounded-b-none",
  "focus-visible:ring-4 ring-indigo-500 ring-inset",
  "ring-offset-2 dark:ring-offset-slate-900",
  "cursor-pointer p-3 transition-all outline-none",
];

const collapseContentBase = [
  "flex flex-col",
  "border-t-2 border-t-transparent",
  "group-open:border-gray-300 dark:group-open:border-gray-600",
  "divide-y-2 divide-gray-300 dark:divide-gray-600",
  "scale-90 opacity-0 group-open:scale-100 group-open:opacity-100 ",
  "transition-all",
];

// const actionBtnProps = {
//   size: "none",
//   className: "h-10 w-10 shrink-0 sm:h-auto sm:w-auto sm:px-3 sm:py-1.5",
// };

// const layout = {
//   list: {
//     header: "md:flex-row",
//     subTasks: "col-span-2 md:col-span-1 md:col-start-2",
//     description: "col-span-2 md:col-span-1 md:col-start-2",
//   },
//   grid: {
//     subTasks: "col-span-2",
//     description: "col-span-2",
//   },
// };

export default function TodoItem({ todo = {}, _onDelete, _onDeleteSubTodo, _onEdit }) {
  // TODO: Get the state from the useTodoControls to apply different styles based on the view mode

  return (
    <li className='relative grid auto-rows-min grid-cols-[auto_1fr] items-start gap-4 rounded-xl bg-white p-4 pt-6 transition-transform dark:bg-slate-800'>
      {/* Add the priority badge to the card */}
      {/* Add the Toggle for completed and not completed and implement the functionality necessary to be able to toggle it */}
      <div
        className={cn(
          "flex flex-col items-start gap-2"
          // layout[state.viewMode]?.header
        )}
      >
        {/* Add a <label> element that can toggle the completed status of the todo and style it with a line though if it is completed */}
        {/* 
          Add 2 buttons:
          1. Link to the edit page
          2. Delete button that will delete the todo
        */}
      </div>
      {/* Show the todo description if there is any */}
      {/* Show the sub todos in a collapsible ( details / summary HTML element ) if there are any */}
      {Boolean(todo.subTasks?.length) && (
        <details
          className={cn(
            // layout[state.viewMode]?.subTasks,
            collapseBase
          )}
        >
          <summary className={cn(collapseTriggerBase)}></summary>
          <div className={cn(collapseContentBase)}>
            {/* You have to map through all the subTodos and render the SubTodoItem component and pass the necessary methods to include the interactivity */}
          </div>
        </details>
      )}
    </li>
  );
}

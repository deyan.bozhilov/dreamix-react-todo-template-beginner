import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import cn from "classnames";

import { Button, Card } from "../../../components";
import { Select } from "../../../components/forms";

export default function TodoForm({ defaultValues, isLoading, onSubmit, title, children }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
    // control, // We use this for the sub todos
  } = useForm({
    defaultValues,
  });

  // For the sub todos we use the useFieldArray hook
  // This hook is used to dynamically add and remove fields
  // Yoy can follow the examples from the documentation and implement it https://react-hook-form.com/api/usefieldarray
  // const { fields, append, remove } = useFieldArray({
  //   control,
  //   name: "subTasks",
  // });

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={cn("py-8", isLoading && "pointer-events-none select-none opacity-50")}>
      <Card className='flex flex-col gap-2 rounded-xl p-4 md:p-10 md:pt-4'>
        {/* We provide the ability to pass a different title for create and edit of todo */}
        {title && <h1 className='my-4 text-2xl font-semibold md:text-4xl'>{title}</h1>}
        <Select
          label='Priority*'
          error={errors?.priority}
          {...register("priority", {
            required: { value: true, message: "Field is required!" },
          })}
        >
          {/* We also provide the three available options for setting the priority of a todo */}
          <option value='low'>Low</option>
          <option value='mid'>Mid</option>
          <option value='high'>High</option>
        </Select>
        {/* 
          TODO: We need a title, which is required and conforms to the validation rules set in the API ( check the ticket )
          NOTE: We use the Input component from the components/forms folder and pass a autoFocus prop to it, which will focus the input on page load
        */}
        {/* 
          TODO: We need a description, which is required and conforms to the validation rules set in the API ( check the ticket )
        */}
        {/* 
          TODO: We need a section for creating sub todos. We will use the useFieldArray hook for this. Check the comments above
        */}
        {children}
        <div className='flex flex-col-reverse flex-wrap gap-4 pt-2 sm:flex-row sm:items-center sm:justify-end md:pt-6'>
          <Button as={Link} to='/' type='button' variant='danger'>
            Cancel
          </Button>
          <Button isLoading={isLoading}>Submit</Button>
        </div>
      </Card>
    </form>
  );
}

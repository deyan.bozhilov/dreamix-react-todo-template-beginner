// A bit of CSS to make your life easier

// const switchWrapper = [
//   "flex gap-2 sm:items-center items-start md:grow-0 grow justify-center shrink-0 sm:flex-row flex-col sm:w-auto w-full",
//   "border dark:border-slate-600 border-slate-300",
//   "p-2 rounded-xl",
// ];

// NOTE: Only if using the details / summary HTML element
// const filterToggle = [
//   "flex items-center gap-2 rounded-lg",
//   "focus-visible:ring ring-indigo-500",
//   "ring-offset-4 dark:ring-offset-slate-900",
//   "cursor-pointer select-none outline-none [&::-webkit-details-marker]:hidden",
// ];

export default function Controls() {
  // const { state, dispatch, isTouched } = useTodoControls();
  // TODO: This will be our filter controls
  // Here you need to use the useTodoControls hook to get the state and dispatch function and manipulate the filters

  // In the demo website: https://earnest-speculoos-d738a7.netlify.app/ on the dashboard the section can be toggled by clicking on the filter icon and title.
  // This is optional behavior, but it is a good practice with some more uncommon HTML element.

  // The filter section should have a reset button, which will reset all the filters to their default values.
  // The reset button should only be visible when the user has changed any of the filters. (see the context and what it provides)
  // You must have the input for searching by title
  // You must have the switch for completed status
  // You must have the select for filtering by priority
  // You must have the switch for view mode

  // NOTE: Don't forget to make it responsive :)

  return <>Todo Controls</>;
}

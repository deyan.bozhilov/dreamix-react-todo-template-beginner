// const layout = {
//   list: {
//     className: "flex flex-col gap-6",
//   },
//   grid: {
//     className: "grid gap-x-4 gap-y-6",
//     style: { gridTemplateColumns: "repeat(auto-fit, minmax(min(20rem, 100%), 1fr))" },
//   },
// };

export default function TodoList({ _todos, _className }) {
  //                               ^       ^
  // NOTE: using _ before the prop name just to trick the eslint, you have to remove it

  // TODO: As you are implementing a lot of features, we have extracted the TodoList component from the
  // Dashboard page and contained the logic for deleting and editing todos and subTodos in the TodoList component
  // You need to use the hooks you have created to implement the delete and edit functionality

  // You need the state from the useTodoControls hook to get the viewMode
  // and can use the viewMode to determine the layout of the list
  // NOTE: We have provided the layout object for you with all the styles and classNames so you can use them

  // If there are no passed in todos from the props, you should show a message that there are no todos

  // Otherwise, you should map through all the todos and render the TodoItem component for each todo, passing the available methods as props
  // NOTE: Don't forget the key prop!
  return <>Todos are supposed to be here</>;
}

import cn from "classnames";

export default function Spinner({ className }) {
  return (
    <div className={cn("aspect-square animate-spin rounded-full border-indigo-300/50 border-t-indigo-600", className || "w-14 border-8")} />
  );
}

import cn from "classnames";

export default function Container({ className, children }) {
  return <div className={cn("mx-auto w-full max-w-4xl", className)}>{children}</div>;
}

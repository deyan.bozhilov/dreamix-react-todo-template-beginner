import { Navigate } from "react-router-dom";

import { useAuth } from "../../context";
import { getTokenFromStorage } from "../../utils";

export default function PrivateRoute({ children }) {
  const [{ token }] = useAuth();

  // Also checking in local storage as the Auth provider needs to re-render
  // to update state, which triggers the if statement and redirects to the
  // dashboard page
  if (!token && !getTokenFromStorage()) {
    return <Navigate to='/login' replace />;
  }
  return children;
}

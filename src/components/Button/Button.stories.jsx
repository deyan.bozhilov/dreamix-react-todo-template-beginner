import Button from "./Button";

export default {
  title: "components/Button",
  component: Button,
  argTypes: { onClick: { action: "onClick" } },
};

export const Example = {
  args: {
    variant: "primary",
    size: "default",
    isLoading: false,
    as: "button",
    className: "",
    children: "This is a button!",
  },
};

import cn from "classnames";
import PropTypes from "prop-types";

const variants = {
  primary: "bg-indigo-600 text-white ring-indigo-600 border-transparent hover:bg-indigo-700",
  secondary: "bg-orange-400 text-black ring-orange-400 border-transparent hover:bg-orange-500",
  danger: "bg-red-600 text-white ring-red-600 border-transparent hover:bg-red-700",
  outline: [
    "bg-white dark:bg-slate-800 text-indigo-600 dark:text-indigo-400",
    "dark:ring-indigo-500 ring-indigo-500 dark:border-indigo-500 border-indigo-600",
    "dark:hover:bg-indigo-900 hover:bg-indigo-100",
  ],
};

const sizes = {
  default: "py-4 px-6",
  sm: "py-2 px-4",
};

export default function Button({
  className,
  variant = "primary",
  size = "default",
  isLoading,
  children,
  as: Component = "button",
  ...rest
}) {
  return (
    <Component
      className={cn(
        [
          "inline-flex items-center justify-center gap-1",
          "relative",
          "select-none",
          "rounded-md border-2",
          "text-base font-semibold leading-none",
          "outline-none ring-offset-2 focus-visible:ring dark:ring-offset-slate-900",
          "transition-all active:scale-95",
          "disabled:cursor-not-allowed disabled:opacity-50",
        ],
        variants[variant],
        sizes[size],
        className
      )}
      {...rest}
    >
      {isLoading && (
        <span className={cn("absolute inset-0 grid place-items-center rounded-md bg-opacity-80", variants[variant])}>
          <span className='block h-5 w-5 animate-spin rounded-full border-4 border-transparent border-t-current' />
        </span>
      )}
      {children}
    </Component>
  );
}

Button.propTypes = {
  variant: PropTypes.oneOf(["primary", "secondary", "danger", "outline", "none"]),
  size: PropTypes.oneOf(["default", "sm", "none"]),
  isLoading: PropTypes.bool,
  as: PropTypes.oneOfType([PropTypes.string, PropTypes.node, PropTypes.any]),
  className: PropTypes.string,
};

import { ThemeProvider, TodoControlsProvider } from "../../context";
import ThemeSwitch from "../../pages/layout/components/ThemeSwitch";
import { CompletedSwitch, ViewModeSwitch } from "../../pages/todos/components";
import { Check, X } from "../icons";

import Switch from "./Switch";

export default {
  title: "components/Switch",
  component: Switch,
};

const Template = (args) => (
  <div className='inline-block bg-white p-4 dark:bg-slate-800'>
    <Switch {...args} />
  </div>
);

export const Example = {
  args: {
    options: [
      { icon: Check, activeCondition: true },
      { icon: X, activeCondition: false },
    ],
    indicatorClassName: {
      "translate-x-0": true,
      "translate-x-10": false,
    },
    className: "",
  },
  render: Template,
};

const ThemeTemplate = (args) => (
  <ThemeProvider>
    <div className='inline-block bg-white p-4 dark:bg-slate-800'>
      <ThemeSwitch {...args} />
    </div>
  </ThemeProvider>
);

export const ThemeExample = {
  args: {
    className: "",
  },
  render: ThemeTemplate,
};

const ViewTemplate = (args) => (
  <TodoControlsProvider>
    <div className='inline-block bg-white p-4 dark:bg-slate-800'>
      <ViewModeSwitch {...args} />
    </div>
  </TodoControlsProvider>
);

export const ViewExample = {
  args: {
    className: "",
  },
  render: ViewTemplate,
};

const CompletedTemplate = (args) => (
  <TodoControlsProvider>
    <div className='inline-block bg-white p-4 dark:bg-slate-800'>
      <CompletedSwitch {...args} />
    </div>
  </TodoControlsProvider>
);

export const CompletedExample = {
  args: {
    className: "",
  },
  render: CompletedTemplate,
};

import cn from "classnames";
import PropTypes from "prop-types";

const base = [
  "transition-all",
  "w-8 h-8",
  "grid place-items-center",
  "outline-none focus-visible:ring ring-offset-2 ring-indigo-600 dark:ring-offset-slate-900",
  "active:scale-95 hover:bg-black hover:bg-opacity-10 dark:hover:bg-slate-200 dark:hover:bg-opacity-10",
  "rounded-lg border-2 border-transparent focus-visible:border-indigo-600",
];
const iconClass = "w-5 h-5";

export default function Switch({ options = [], keyPrefix = "switch", className, indicatorClassName }) {
  return (
    <div className={cn("relative isolate inline-flex h-min gap-2 rounded-xl bg-slate-200 p-2 dark:bg-slate-900", className)}>
      <div className={cn(base, "pointer-events-none absolute -z-10 bg-indigo-500 transition-transform", indicatorClassName)} />
      {options.map((option, index) => {
        const Icon = option.icon;
        return (
          <button
            key={`${keyPrefix}_${index}`}
            className={cn(base, option.activeCondition && "text-white")}
            onClick={option.onClick}
            title={option.title}
          >
            <Icon className={iconClass} />
          </button>
        );
      })}
    </div>
  );
}

Switch.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      icon: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
      activeCondition: PropTypes.bool,
      onClick: PropTypes.func,
      title: PropTypes.string,
    })
  ).isRequired,
  indicatorClassName: PropTypes.object,
  keyPrefix: PropTypes.string,
  className: PropTypes.string,
};

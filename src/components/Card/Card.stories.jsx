import Card from "./Card";

export default {
  title: "components/Card",
  component: Card,
};

export const Example = {
  args: {
    className: "rounded-xl p-10",
    children: "This is a card!",
  },
};

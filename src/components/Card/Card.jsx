import cn from "classnames";

export default function Card({ className, children }) {
  return (
    <article
      className={cn([
        "border-2 shadow-xl",
        "border-slate-300 dark:border-slate-700",
        "bg-white dark:bg-slate-900 dark:text-white",
        className,
      ])}
    >
      {children}
    </article>
  );
}

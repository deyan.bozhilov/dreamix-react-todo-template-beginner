import cn from "classnames";

const base = ["rounded-md px-4 py-2 leading-snug", "bg-red-600 text-white"];

export default function Alert({ className, children }) {
  return <div className={cn(base, className)}>{children}</div>;
}

import Alert from "./Alert";

export default {
  title: "components/Alert",
  component: Alert,
};

export const Example = {
  args: {
    className: "",
    children: "This is an alert!",
  },
};

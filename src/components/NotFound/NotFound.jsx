import { Link } from "react-router-dom";

import { Button } from "../Button";

export default function NotFound({ title }) {
  return (
    <div className='flex flex-col items-center justify-center gap-8 py-10'>
      <p className='text-2xl font-bold'>{title ?? "Page Not Found!"}</p>
      <Button as={Link} to='/'>
        Back to Dashboard
      </Button>
    </div>
  );
}

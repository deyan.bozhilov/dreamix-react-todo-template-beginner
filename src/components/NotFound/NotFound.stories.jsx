import { BrowserRouter } from "react-router-dom";

import NotFound from "./NotFound";

export default {
  title: "components/NotFound",
  component: NotFound,
};

const Template = (args) => (
  <BrowserRouter>
    <NotFound {...args} />
  </BrowserRouter>
);

export const Example = {
  args: {
    title: "You lost?",
  },
  render: Template,
};

import { forwardRef } from "react";

import { baseInputPropTypes } from "./propTypes";
import { withInputWrapper } from "./withInputWrapper";

function Input(props, ref) {
  return <input type='text' {...props} ref={ref} />;
}

export default withInputWrapper(forwardRef(Input));

Input.propTypes = baseInputPropTypes;

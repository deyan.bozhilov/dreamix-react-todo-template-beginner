import { forwardRef, useState } from "react";

import { Button } from "../../Button";
import { Eye, EyeOff } from "../../icons";

import { baseInputPropTypes } from "./propTypes";
import { withInputWrapper } from "./withInputWrapper";

function Password(props, ref) {
  const [passwordVisible, setPasswordVisible] = useState(false);
  return (
    <div className='relative'>
      <input autoComplete='true' {...props} type={passwordVisible ? "text" : "password"} ref={ref} />
      <div className='absolute inset-y-0 right-2 grid place-items-center'>
        <Button
          size='none'
          variant='outline'
          className='p-1'
          type='button'
          onClick={() => setPasswordVisible((prev) => !prev)}
          title='Toggle password visibility'
        >
          {passwordVisible ? <EyeOff className='h-4 w-4' /> : <Eye className='h-4 w-4' />}
        </Button>
      </div>
    </div>
  );
}

export default withInputWrapper(forwardRef(Password));

Password.propTypes = baseInputPropTypes;

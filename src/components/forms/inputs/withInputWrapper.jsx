import { forwardRef } from "react";
import cn from "classnames";

import ErrorLabel from "../ErrorLabel";
import Label from "../Label";

import { baseInputClasses, borderColor } from "./const";

export function withInputWrapper(Component) {
  // We can use the withInputWrapper function to wrap any input component with the same wrapper.
  function InputWrapper({ label, groupClassName, error, className, ...rest }, ref) {
    // We can use the rest operator to get the props that we don't need to use in the wrapper.
    const { name, id = name } = rest ?? {};
    return (
      <div className={cn("grid gap-1", groupClassName)}>
        <Label {...{ id, error, label }} />
        {/* We render the input component here and pass the rest of the props to it. */}
        <Component className={cn(baseInputClasses, borderColor(error), className)} {...{ ...rest, name, id }} ref={ref} />
        {Boolean(error) && <ErrorLabel error={error} />}
      </div>
    );
  }

  // ❗We need to use forwardRef to be able to use the ref prop on the input component.
  return forwardRef(InputWrapper);
}

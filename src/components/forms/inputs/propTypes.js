import PropTypes from "prop-types";

export const baseInputPropTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  groupClassName: PropTypes.string,
  error: PropTypes.shape({ message: PropTypes.string }),
  className: PropTypes.string,
};

export const baseInputClasses = [
  "block w-full",
  "rounded-md",
  "transition-shadow",
  "focus:ring focus:ring-offset-2",
  "bg-slate-100 focus:bg-white dark:bg-slate-800 dark:focus:bg-slate-700 dark:placeholder:text-slate-400 dark:ring-offset-slate-900",
];

export const borderColor = (error) => {
  if (error) {
    return "border-red-500 focus:border-red-600 focus:ring-red-600 dark:focus:ring-red-500";
  }
  return "border-gray-300 dark:border-gray-600 focus:border-indigo-500 focus:ring-indigo-500";
};

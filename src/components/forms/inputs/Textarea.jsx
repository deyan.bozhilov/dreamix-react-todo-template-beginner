import { forwardRef } from "react";
import cn from "classnames";

import { baseInputPropTypes } from "./propTypes";
import { withInputWrapper } from "./withInputWrapper";

function Textarea({ className, ...rest }, ref) {
  return <textarea {...rest} className={cn("min-h-[7rem]", className)} ref={ref} />;
}

export default withInputWrapper(forwardRef(Textarea));

Textarea.propTypes = baseInputPropTypes;

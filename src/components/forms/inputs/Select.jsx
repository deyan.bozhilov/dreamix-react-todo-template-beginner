import { forwardRef } from "react";

import { baseInputPropTypes } from "./propTypes";
import { withInputWrapper } from "./withInputWrapper";

function Select(props, ref) {
  return <select {...props} ref={ref} />;
}

export default withInputWrapper(forwardRef(Select));

Select.propTypes = baseInputPropTypes;

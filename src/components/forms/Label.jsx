import cn from "classnames";

export default function Label({ id, error, label }) {
  return (
    <label
      htmlFor={id}
      className={cn("ml-3 font-semibold", error ? "text-red-600 dark:text-red-500" : "text-gray-700 dark:text-slate-200")}
    >
      {label}
    </label>
  );
}

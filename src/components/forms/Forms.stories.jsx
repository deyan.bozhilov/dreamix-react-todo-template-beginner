import { Input, Password, Select, Textarea } from "./inputs";

export default {
  title: "components/forms",
};

const inputProps = {
  name: "name",
  label: "Name",
  placeholder: "Enter name",
  className: "",
};

const renderInput = (args) => (
  <div className='inline-block bg-white p-4 dark:bg-slate-800'>
    <Input {...args} />
  </div>
);

export const InputExample = {
  args: inputProps,
  render: renderInput,
};

export const InputExampleError = {
  args: {
    ...inputProps,
    error: { message: "Field is required" },
  },
  render: renderInput,
};

const renderPassword = (args) => (
  <div className='inline-block bg-white p-4 dark:bg-slate-800'>
    <Password {...args} />
  </div>
);

const passwordProps = {
  name: "random",
  label: "Password",
  placeholder: "Enter password",
  className: "",
};

export const PasswordExample = {
  args: passwordProps,
  render: renderPassword,
};

export const PasswordExampleError = {
  args: {
    ...passwordProps,
    error: { message: "Field is required" },
  },
  render: renderPassword,
};

const renderSelect = (args) => (
  <div className='inline-block min-w-[20rem] bg-white p-4 dark:bg-slate-800'>
    <Select {...args} />
  </div>
);

const selectProps = {
  name: "select",
  label: "Priority",
  className: "",
  children: (
    <>
      <option value='low'>Low</option>
      <option value='mid'>Mid</option>
      <option value='high'>High</option>
    </>
  ),
};

export const SelectExample = { args: selectProps, render: renderSelect };

export const SelectExampleError = {
  args: {
    ...selectProps,
    error: { message: "Field is required" },
  },
  render: renderSelect,
};

const renderTextarea = (args) => (
  <div className='inline-block min-w-[20rem] bg-white p-4 dark:bg-slate-800'>
    <Textarea {...args} />
  </div>
);

const textareaProps = {
  name: "description",
  label: "Description",
  placeholder: "Enter message",
  className: "",
};

export const TextareaExample = { args: textareaProps, render: renderTextarea };

export const TextareaExampleError = {
  args: {
    ...textareaProps,
    error: { message: "Field is required" },
  },
  render: renderTextarea,
};

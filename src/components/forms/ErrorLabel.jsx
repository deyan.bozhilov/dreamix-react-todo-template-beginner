export default function ErrorLabel({ error }) {
  return <small className='ml-3 mt-1 block leading-tight text-red-600 dark:font-semibold dark:text-red-500'>{error?.message}</small>;
}

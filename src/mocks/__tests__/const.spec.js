import { afterEach, beforeEach, describe, expect, it, vi } from "vitest";

import { getResponseDelay, passwordRegex } from "../const";

describe("password Regex", () => {
  it("should verify that password fails if there is less than 8 characters", async () => {
    const password = "Admin12";
    const result = passwordRegex.test(password);
    expect(result).toBe(false);
  });

  it("should verify that password fails if there is not at least 1 Uppercase character", async () => {
    const password = "admin123";
    const result = passwordRegex.test(password);
    expect(result).toBe(false);
  });

  it("should verify that password fails if there is not at least 1 numeric character", async () => {
    const password = "Adminaaa";
    const result = passwordRegex.test(password);
    expect(result).toBe(false);
  });

  it("should verify that password passes if all criteria are met", async () => {
    const password = "Admin123";
    const result = passwordRegex.test(password);
    expect(result).toBe(true);
  });
});

describe("mocks response delay", () => {
  it("should have 0 sec mock response delay if in testing mode", async () => {
    expect(getResponseDelay()).toBe(0);
  });
});

describe("mocks response delay", () => {
  beforeEach(() => {
    vi.stubEnv("MODE", "not-test");
    vi.stubEnv("REACT_APP_MOCK_RESPONSE_DELAY", "750");
  });

  afterEach(() => {
    vi.unstubAllEnvs();
  });

  it("should have env mock response delay if in testing mode", async () => {
    expect(import.meta.env.MODE).toBe("not-test");

    expect(getResponseDelay()).toBe(750);
  });
});

import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";

import { TodoApi } from "../api";

export const todoKeys = {
  all: () => ["todos"],
  list: () => [...todoKeys.all(), "list"],
  byId: (id) => [...todoKeys.all(), id],
};

export function useTodoAdd({ onSuccess }) {
  // We ge the queryClient from react-query
  const queryClient = useQueryClient();

  // We use the useMutation hook from react-query
  const { mutate, isLoading, error } = useMutation({
    // We pass the mutation function that will be called when the mutate function is called
    mutationFn: TodoApi.add,
    // We pass the onSettled function, that will be called when the mutation is done ( doesn't matter if it was successful or not )
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: todoKeys.list() });
    },
    // We pass the onSuccess function, that will be called when the mutation is successful
    onSuccess,
  });

  return { add: mutate, error, isLoading };
}

// TODO: Add hook to edit todos
// ----------------------------
// Make a hook to edit todos, that will use the useMutation hook from react-query
// The mutate function should call the edit method from the TodoApi
// The hook should return the mutate function, isLoading and error
// The mutate function should invalidate the list query

// TODO: Add hook to delete todos
// ------------------------------
// Make a hook to delete todos, that will use the useMutation hook from react-query
// The mutate function should call the delete method from the TodoApi
// The hook should return the mutate function, isLoading and error
// The mutate function should invalidate the list query

// TODO: Add hook to delete sub todos
// ----------------------------------
// Make a hook to delete sub todos, that will use the useMutation hook from react-query
// The mutate function should call the deleteSubTodo method from the TodoApi
// The hook should return the mutate function, isLoading and error
// The mutate function should invalidate the list query

export function useTodos() {
  const { data, isFetching } = useQuery(todoKeys.list(), TodoApi.getAll);
  return { todos: data, isFetching };
}

// TODO: Add hook to get todo by id
// --------------------------------
// Make a hook to get todo by id, that will use the useQuery hook from react-query
// The query should have the key of the todo by id ( use the todoKeys.byId method )
// The query should call the getById method from the TodoApi
// The hook should return the todo and isFetching
// NOTE: The query should be enabled only if the id is not falsy
// useQuery(... , ... , {
//   enabled: Boolean(id),
//   keepPreviousData: false, // when getting a single todo, we don't want to keep the previous data. We want to show the loading states
//})

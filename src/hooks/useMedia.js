import { useEffect, useState } from "react";

// A hook that will return true or false if the media query matches
// It will also update the state if the media query changes and trigger a re-render
// We use it particularly to check if the OS is set to a dark or light theme,
// but it can be used for any media query

export default function useMedia(query) {
  // We use the useState hook to store the state
  const [state, setState] = useState(() => window.matchMedia(query).matches);

  useEffect(() => {
    // We use the useEffect hook to add a listener to the media query
    // We also return a function to remove the listener
    // We use the mounted variable to make sure that we don't call setState if the component is unmounted
    let mounted = true;
    // We get the media query list from the window
    const mql = window.matchMedia(query);

    function onChange() {
      // We call setState only if the component is mounted
      if (!mounted) return;
      setState(mql.matches);
    }

    mql.addListener(onChange);
    setState(mql.matches);

    return () => {
      // We set mounted to false when the component is unmounted
      mounted = false;
      mql.removeListener(onChange);
    };
  }, [query]);

  return state;
}

import {
  useMutation,
  // useQuery
} from "@tanstack/react-query";

import { AuthApi } from "../api";

// This is a helper object to create an array of keys for the useQuery hook.
// It is used to invalidate the cache when the user logs in or out.
// and can be applied to any other query that needs to be invalidated
export const authKeys = {
  all: () => ["auth"],
  account: () => [...authKeys.all(), "account"],
};

export function useRegister({ onSuccess }) {
  const { mutate, isLoading, error } = useMutation({ mutationFn: AuthApi.register, onSuccess });
  return { register: mutate, error, isLoading };
}

// TODO: Implement the useLogin hook
// It should use the useMutation hook to call the login method from the AuthApi
// It should return the mutate function, the error and the isLoading
// 👨‍💻👩‍💻 Your turn! Implement the useLogin hook.

// After the login/register is successful and we have stored the token in localStorage,
// we can get the current user's account information and store it in the auth context
// ( we will do this in a component and we will not couple the logic with the actual retrieval of the data )

// export function useAccount(config) {
//   const { data, isFetching } = useQuery(authKeys.account(), AuthApi.getAccount, config);
//   return { account: data, isFetching };
// }

// Replace me 🙏
export function useAccount() {
  return {};
}

// export function useLogout({ onSuccess }) {
//   return {
//     logout: () => AuthApi.logout().then(onSuccess),
//   };
// }

// Replace me 🙏
export function useLogout() {
  return {};
}

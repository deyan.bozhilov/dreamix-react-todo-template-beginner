import { useCallback, useState } from "react";

// A helper hook, that will store a value in localStorage and return it
// It will also update the value in localStorage if the value changes
// It will also remove the value from localStorage if the removeValue function is called
// We use it with a hook, because it has state internally and when values change, we want to re-render
// the component it used in

export default function useLocalStorage(key, initialValue) {
  const [storedValue, setStoredValue] = useState(() => {
    // We try to get the value from localStorage
    try {
      const item = localStorage.getItem(key);
      // If the value is not null, we parse it and return it
      return item ? JSON.parse(item) : initialValue;
    } catch (err) {
      // If there is an error, we return the initialValue
      return initialValue;
    }
  });

  const setValue = (value) => {
    // We try to set the value in localStorage
    try {
      // If the value is a function, we call it with the storedValue and use the result as the value
      const valueToStore = value instanceof Function ? value(storedValue) : value;
      // We set the value in state
      setStoredValue(valueToStore);
      // We set the value in localStorage
      localStorage.setItem(key, JSON.stringify(valueToStore));
    } catch (err) {
      // If there is an error, we return null
      return null;
    }
  };

  const removeValue = useCallback(() => {
    // We try to remove the value from localStorage
    try {
      localStorage.removeItem(key);
      // We set the value in state to the initialValue
      setStoredValue(initialValue);
    } catch (err) {
      return null;
    }
  }, [key, initialValue]);

  return [storedValue, setValue, removeValue];
}

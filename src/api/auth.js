// import axios from "./axios";
// import { clearToken } from "./utils";

export default {
  // // TODO: Add API methods here
  // //
  // // Example:
  // //
  // // methodName: (args) => axios.method("/path", args).then(({ data }) => data),
  // // ^---------^ ^----^    ^----------^ ^-----^  ^---^ ^----------------------^
  // //               👆           👆          👆      👆             👆
  // // |things you pass in||method to call||path to||data to send||data to return|
  // // |as a body or just ||could be get, ||call   ||only for put|
  // // |a single arg for  ||post, put,    ||       ||post, patch |
  // // |dynamic url       ||delete, etc.  |
  // //
  // // Before we can start seeing or creating our todos, we need to login or register.
  // // We'll start by adding a register method to our AuthApi.
  // register: (body) => axios.post("/register", body).then(({ data }) => data),
  // //
  // // Now we can call AuthApi.register({ email: "email", password: "password" }) to register a user.
  // // After we register a user, we want to be able to login. So we can do that the same way, just with a different path.
  // //
  // // 👨‍💻👩‍💻 Your turn! Add a login method to the AuthApi.
  // //
  // // Ok, so we have logged in, but that doesn't mean we have the logged in user yet.
  // // We have to get our now logged in user from the server.
  // // After setting the token in the Authorization Header, the server now knows who we are.
  // // So we can just get all the data for that account.
  // getAccount: () => axios.get("/account").then(({ data }) => data),
  // //
  // // After all is done and we have finished our todos, we want to be able to logout.
  // // So we just call a logout method to clear the token from localStorage, which then can be
  // // used to logout the user if there is no token available anymore.
  // logout: clearToken,
  //
  // 🎉💯🎉 All done! You can now login, register, logout and get your account data. 🎉💯🎉
};

import { getTokenFromStorage } from "../utils";

const tokenKey = import.meta.env.REACT_APP_TOKEN_KEY;

export function setAuthHeadersOnRequest(config) {
  try {
    // Here we try to get and parse the token from localStorage
    const token = getTokenFromStorage();
    // If we have a token, we set the Authorization header to Bearer <token>
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  } catch (error) {
    // If we can't parse the token, we reject the promise
    return Promise.reject(error);
  }
}

export function removeTokenFromStorageIfUnauthorizedResponse(error) {
  try {
    // If we get a 401 response, we remove the token from localStorage
    // and reload the page to force the user to login again
    if (error.response?.status === 401) {
      localStorage.removeItem(tokenKey);
      window.location.reload(true);
    }
    return Promise.reject(error);
  } catch (error) {
    return Promise.reject(error);
  }
}

export function clearToken() {
  // We return a promise here so we can use async/await in our components to remove the token
  return new Promise((resolve) => resolve(localStorage.removeItem(tokenKey)));
}

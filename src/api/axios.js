import axios from "axios";

// import { removeTokenFromStorageIfUnauthorizedResponse, setAuthHeadersOnRequest } from "./utils";

const instance = axios.create({});
// After creating the instance, we can add interceptors to it, which will run before every request and response.
// We want to add the user token to the Authorization header of every request, so we'll add an interceptor to add the token to the request.
// ❗This is important because most of our requests need the token to be able to access the data.❗
// instance.interceptors.request.use(doSomethingToRequest);

// Also we want to remove the token from localStorage and logout the user if we get a 401 response, so we'll add an interceptor to do that.
//
// instance.interceptors.response.use(null, doSomethingToResponse);

export default instance;

// import axios from "./axios";

export default {
  // // TODO: Add API methods here
  // //
  // // Example:
  // //
  // // methodName: (args) => axios.method(`/path/${args.id}`, args).then(({ data }) => data),
  // // ^---------^ ^----^    ^----------^ ^----------------^  ^---^ ^----------------------^
  // //               👆          👆                 👆           👆             👆
  // // |things you pass in||method to call||path to call on||data to send||data to return|
  // // |as a body or just ||could be get, ||               ||only for put|
  // // |a single arg for  ||post, put,    ||               ||post, patch |
  // // |dynamic url       ||delete, etc.  |
  // //
  // // Before we can start seeing our todos, we need to be able to create them.
  // // We'll start by adding a create method to our TodoApi.
  // create: (todo) => axios.post("/todos", todo).then(({ data }) => data),
  // //
  // // Now we can call TodoApi.create({ title: "My first todo", ... }) to create a todo.
  // // After we create a todo, we want to be able to see it.
  // // We'll start by adding a get methods to our TodoApi.
  // // At first we can get all of our todos, but later we'll add a way to get a single todo.
  // getAll: () => axios.get("/todos").then(({ data }) => data),
  // //
  // // 👨‍💻👩‍💻 Your turn! Add a getById method to the TodoApi.
  // //
  // // Now we can call TodoApi.getAll() to get all of our todos.
  // // We can also call TodoApi.getById(1) to get a single todo with an id of 1.
  // //
  // // After all of that work, we want to be able to edit and delete our todos.
  // // We'll start by adding a update method to our TodoApi.
  // // update url: /todos/:id (PUT) and we need to provide the whole todo object
  // //
  // // 👨‍💻👩‍💻 Your turn! Add a update method to the TodoApi.
  // //
  // // Then we'll add a delete method to our TodoApi.
  // // delete url: /todos/:id (DELETE)
  // //
  // // And finally we need to be able to delete subTodos.
  // // delete url: /todos/:id/:subTodoId (DELETE)
  // //
  // // 👨‍💻👩‍💻 Your turn! Add delete methods to the TodoApi.
  // //
  // // 🎉💯🎉 All done! You can now create, get, update and delete todos. 🎉💯🎉
};

export function setTokenInStorage(token) {
  try {
    // We stringify the token here so we can parse it later
    localStorage.setItem(import.meta.env.REACT_APP_TOKEN_KEY, JSON.stringify(token));
  } catch (error) {
    return null;
  }
}

export function getTokenFromStorage() {
  try {
    // We parse the token here so we can use it as a string
    const token = localStorage.getItem(import.meta.env.REACT_APP_TOKEN_KEY);
    return token ? JSON.parse(token) : null;
  } catch (error) {
    return null;
  }
}
